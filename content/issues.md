+++
title = 'The Issues'
date = 'July 19, 2020'
toc=true
+++

This page lists the issues we face in modern society followed by the solution. 
Every claim made is backed by evidence, view the references section to view primary information.
If you notice any errors please contact us on the [About Us]({{<relref "about-us.md">}}) page.
{{<toc>}}

## Economic Issues

### Homelessness
**Problem**

We live in a society in which we label ourselves as a "developed" nation.
We have prescribed ourselves this label because we are constantly presented with the image of our elite living in luxury.
The image of luxury is impressed on us throughout our daily lives to mask the cruel realities of those living at the bottom strata of society.


The homeless are an example of the rare few in our society who face their mortality on a daily basis.
We are afraid of them because they smell, they may not have great social skills or they may appear disfigured, but this is no fault of their own, every day for them is a fight to survive.
You cannot worry about your public appearance when you are facing your own death.

Homelessness is such an uncomprehensible torture that the only way for us to accept it is to rationalize it's existence.
We are led to believe that the result of homelessness is the fault of the victim.
We victim blame those in our society who have the least and denigrate them for having nothing.

We label the homeless as "bums" or "vagrants" but the truth is, severe poverty is incredibly difficult to escape.
As was mentioned earlier, the homeless face their mortality every single day, how can you find employment if you must spend your entire day meeting your immediate needs, acquiring food, water, shelter, warmth?
How can one acquire employment if they have no access to proper hygiene (shower, deodorant, dental health, hair management), professional or even casual attire, a bank account for payroll, an address, tax records, identification, a resume?
Many of the homeless suffer from physical or mental health issues, histories of physical and sexual abuse, familial and social rejection, but how can you recieve the support of OHIP or government assistance without ID, an address, a bank account, tax records, etc?
We expect these people in our society to somehow resolve these contradictions, to escape this endless cycle, it is absurd.

Now some may say that it is such a small minority in our society it is a non issue. 
But this is not the case, so many people experience homelessness yet we ignore their suffering because they are shuffled away from our view.
It is estimated that over 200,000 Canadians a year experience homeless in any year[^StateOfHomelessnessCanada].
The large number of people one can easily witness in our large urban centers are only a small fraction of the actual homeless population.
The police and municipal employees are constantly moving the homeless around to places out of view so we can continue to ignore this systemic issue, moving them to tent cities under bridges, parking lots in industrial areas.


The homeless are the ultimate symbol of the decay of capitalism and neo-liberal policies.
Surrounded by wealth and luxury but forced to scavenge for survival.
Demonized by the elite for their inescapable condition.
Living in a "democracy" yet lacking any representation.

**Solution**

The solution for the homeless plight is a comprehensive strategy deeply connected with the solution to [unemployment]({{<relref "#unemployment">}}).

The primary front of the solution is to remove the idea of housing as a commodity.
As human beings we have a right to shelter, a right to live in warmth, housing should not be a commodity that people trade for profit.
It is a common misconception that homelessness is due to a lack of housing, this is not the case, there are more empty homes then there are homeless[^HomelessHouseCount].
Land ownership should not be a source of passive income for the landlord, the landlord exhibits a parasitic relationship where they extract passive income from their tenants.
Land ownership/housing is to be a right with which every person/family is to be entitled to.

Housing as a right technically solves the homelessness crisis but it is not a complete strategy.
The homeless will be guaranteed a job as is represented in the [unemployment]({{<relref "#unemployment">}}) section but this still is not complete.
The trauma that the homeless face on the streets is significant, adjusting to a working, modern lifestyle will be difficult for many of these people.
A task force of counselors and support workers will be commisioned to support these newly liberated citizens and meet with them regularly to determine their needs and how to best reintegrate them into society.
Many will require part or full time [mental health support]({{<relref "#mental-health-care">}}) as they adjust to their new environments.
Many may require adult education so they can develop the social and everyday skills for professional interaction.

The solution described here is no small undertaking, significant organization will be required to solve this ailment.
But it is necessary, we cannot live in a two tiered society where some live in comfort while others fight to survive.
We will be liberating a people which have never been liberated in this country before.
This will also be of tremendous benefit to society, adding more workers to the workforce, increasing productivity, lowering [crime]({{<relref "#crime">}}) rates, decreasing the [drug]({{<relref "#drugs">}}) problem, these effects will be felt for generations to come.

It is time to end the homeless plight, this is the solution, it is simple yet no small task, if we can put humans into orbit we can provide our people with housing.

### Unemployment
**Problem**

Unemployment is a confusingly simple yet barbaric issue.
The issue is people do not have employment, they lack the ability to contribute to society and society punishes them by not allowing them to eat.
Why do we not allow people to contribute to society when we face so many issues?


Every time you face an understaffed location or excessive lines you are witnessing a failure of the economic system.
Capitalism has been lauded for providing quality services at a low price due to it's competitive nature, but this is clearly not the case.
The capitalists' only goal is to extract profit, they understand that supply and demand is effectively dead, we live in the age of oligopolies and functional monopolies.
The consumer usually has few options, so why should the capitalist provide a good service when providing better service will only diminish their profits?
Private enterprise has no incentive to employ beyond the bare minimum in our society.


We also face this issue in the public sector, long hospital waiting periods, large class sizes, government waiting lines.
The government is infamous in the west for being slow and ineffective.
State enterprises face an extremely restricted budget resulting in minimal wages which causes high value employees to flock to the private market and low employment numbers.
The reason the state enterprises face these issues is due to the financial model the state has developed as discussed in the [debt]({{<relref "#debt">}}) section.

Our unemployment rate throughout the past few decades can be seen below.

{{<figure src="/images/canadian_unemployment_rate.png" title="Unemployment Rate Canada 1976-2016" alt="Unemployment Rate Canada 1976-2016" caption="Retrieved from [Statistics Canada](https://www150.statcan.gc.ca/n1/daily-quotidien/170707/cg-a003-png-eng.htm)">}}
The above figure demonstrates the clear size of our unemployment problem, between 5.7% to up to 12% of people at any time are able and are looking for employment but cannot recieve it.

Our society would be more productive, more equitable and provide the downtrodden with hope if we were to end unemployment.

**Solution**

The solution for unemployment is quite simple, employ those who are ablebodied.
Employment should be a right and a responsibility.
Rather than providing Employment Insurance or welfare to those who cannot find a job, why not simply provide them with a job?
There is no shortage of tasks to complete.
Our society should be revolve around this statement "*From each according to his ability, to each according to his need*"[^GothaCritique].
Every person should provide what they can to society, and recieve what they require.
A more detailed explanation of the economic program is provided on the solutionXXXXX INSERT LINK HERE XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx page

### Debt

**Problem**

INSERT HERE........................................
**Solution**

### Low Wages / Wage Theft / Surplus Value

**Problem**
In our current economic system, the worker recieves minimal pay.
The worker is a dialectic(contradiction) to the employer in their wage negotiations.
To achieve maximum profit, the employer will pay the worker the minimum amount to retain their labour.
This results in workers recieving compensation which leaves many in desperation and struggle.

The propaganda that we are fed is that as our society develops our wages will rise.
This seems like a reasonable assumption, and is an excellent idea but it has not occured in our society.
{{<figure src="/images/real_wages_growth.gif" title="Canadian Real Hourly Wages 1981-2011" alt="Canadian Real Hourly Wages 1981-2011" caption="Retrieved from [Statistics Canada](https://www150.statcan.gc.ca/n1/pub/11f0019m/2013347/ct001-eng.htm)">}}

As can be seen in the above figure, wages have "roughly" risen in the past 30 years(an entire generation) of about $3 for the average wage, or just about $2 for the median wage. 
A percentage change of about 15% and 10% respectively.
Now one could still argue that wages are technically "rising" as expected, but these wages are still incredibly small and do not reflect the true economic growth we've experienced in the last 30 years as shown below.
{{<figure src="/images/real_gdp_capita_canada.png" title="Real GDP per capita Canada 1981-2011" alt="Real GDP per capita Canada 1982-2011" caption="Figure retrieved from [Federal Reserve Bank of St. Louis](https://fred.stlouisfed.org/series/CANRGDPC), data retrieved from [U.S. Bureau of Labor Statistics](https://www.bls.gov/ilc/intl_gdp_capita_gdp_hour.htm)">}}
It is difficult to measure economic productivity and growth, for this comparison I used Real GDP per capita which is supposed to be a measure of the average economic output per person adjusted for inflation and currency price fluctuations.
Regardless of which exact indicator you use it is clear that the economic growth of the nation has sharply increased yet wages have failed to meet it's pace.
The economic indicator I used grew nearly 50% while wages only grew 10-15%, the worker should have seen 3-5x as much of a wage increase but failed to do so.

The employer at any private institution is effectively stealing the value of a worker's labour through the profit mechanism.

**Solution**

### Poor Working Conditions

### Health Care

### Pensions

### The Changing Job Market (e.x The shrinking Alberta Oil Industry)

## Social Issues

### Policing

### Discrimination

### Mental Health Care

### Mass Surveillance

### Education

#### Large Class Sizes

#### Geographic and Economic Disparity

### Drugs

### Crime

### Something something Sustainable Urban planning, New Urbanism??

## Political Issues

### Failure to adapt to change

### Corruption

### War

### Indigenous Rights

### French Cultural Rights

## References
All references are in APA format.
[^StateOfHomelessnessCanada]:   Stephen Gaetz, Erin Dej, Tim Richter, & Melanie Redman (2016): The State of Homelessness in Canada 2016. Toronto: Canadian Observatory on Homelessness Press.
[^HomelessHouseCount]: Hopulele, A. (2019, August 29). "Ghost" Homes Across Canada: A Decade of Change in 150 Cities. Retrieved July 28,2020 from https://www.point2homes.com/news/canada-real-estate/ghost-homes-across-canada-decade-change-150-cities.html 
[^GothaCritique]: Marx, K., &amp; Engels, F. (1970). Critique of the Gotha programme. Moscow: Progress. Retrieved August 27, 2020, from https://www.marxists.org/archive/marx/works/1875/gotha/index.htm
