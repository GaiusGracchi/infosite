+++
title = 'The Plan'
date = 'July 1,2020'
+++
# The Problem
There are an innumerable number of issues that we face in contemporary society.
We witness the effects of these problems every day but we forget them in time or much worse 
dismiss them as intractable problems. A detailed list of problems are associated with solutions
on the [issues]({{<relref "issues.md">}}) page.
# The Solution
We are lead to believe that our issues are overly complex and intractable, too often this is not the case.
We are not overburdened with issues rather we are overburdened with an inability to solve them.
We must reconstruct our society to solve these systemic issues. *********SEE HERE******* xxx
# Action Plan
